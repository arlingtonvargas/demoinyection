﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interface
{
    public interface IOperationEntities<T>
    {

        //bool Create(T obj);

        //T GetById(string key);

        //bool Update(T obj);

        //bool Delete(string key);

        List<T> GetData();

    }
}
