﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using BL.Interface;
using ET;

namespace appInyection.Service
{
    public class DemoController
    {
        protected IOperationEntities<DemoET> _inter;

        public DemoController(IOperationEntities<DemoET> inter)
        {
            this._inter = inter;
        }

        public List<DemoET> GetData()
        {
            return this._inter.GetData();
        }
    }
}
