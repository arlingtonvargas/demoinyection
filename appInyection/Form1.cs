﻿using appInyection.Service;
using BL;
using ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appInyection
{
    public partial class Form1 : Form
    {
        DemoController demo = new DemoController(new DemoBL());
            

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            demo.GetData();
        }
    }
}
